
// Un peu de code d'initialisation
var game;

// Le code qui est appelé par le navigateur au chargement de la fenêtre contenant le site web
window.onload = function() {

    // on crée une variable gameConfig qui va contenir la configuration générale de notre jeu
    let gameConfig = { 
        scale: { // les dimensions et le positionnement de notre jeu
            autoCenter: Phaser.Scale.CENTER_BOTH, // le jeu sera centré à l'écran
            width: 1024, // le jeu fera 1024 pixels de large
            height: 480 // le jeu fera 480 pixels de haut
        },
        pixelArt: true, // indique que l'on a un jeu en "pixelArt" - on verra plus tard pourquoi / ce que ça veut dire
        scene: playGame // la scène principale du jeu est définie dans l'objet 'playgame' défini ci-dessous
    }

    // on demande à la librairie Phaser de créer "pour de bon" le jeu à partir de la configuration ci-dessus
    game = new Phaser.Game(gameConfig);
}

// La portion de code qui va contenir notre jeu (sera utilisée par Phaser au moment de la création du jeu
// grace au paramètre "scene" de la configuration ci-dessus)
class playGame extends Phaser.Scene {

    // La fonction "create" est appelée une fois au démarrage du jeu
    create(){

        // Phaser ajoute un texte ; le texte est positionné :
        // - en x / horizontal = game.config.width / 2 ; soit la moitié de la largeur du jeu
        // - en y / vertical = game.config.height / 2 ; soit la moitié de la hauteur du jeu
        // - et c'est le centre du texte qui est positionné à cet endroit (setOrigin(0.5, 0.5))
        // - et le texte affiché est 'Bienvenue.'
        this.add.text(game.config.width / 2, game.config.height / 2, 'Bienvenue.').setOrigin(0.5, 0.5);

    }

}