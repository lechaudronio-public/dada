var game;

window.onload = function() {

    let gameConfig = { 
        backgroundColor:0x87ceeb, // On en profite pour changer la couleur de fond du jeu (un bleu clair au lieu du noir)
        scale: {
            autoCenter: Phaser.Scale.CENTER_BOTH,
            width: 1024,
            height: 480
        },
        pixelArt: true,
        scene: playGame 
    }

    game = new Phaser.Game(gameConfig);
}

class playGame extends Phaser.Scene {

    // La fonction "create" est appelée une fois au démarrage du jeu
    create(){

        // A noter : on peut très bien répartir la commande sur plusieurs ligne en insérant des retours
        // chariots entre les "mots" sans aucun impact sur le jeu, Javascript est permissif
        this.add.text(game.config.width / 2, 
                      game.config.height / 2, 
                      'Bienvenue.', 
                      { fontSize: 96, color: '#ff00ff' }) // Afficher en taille 64 et couleur magenta
                 .setOrigin(0.5); // si l'origine en X et en Y est identique, on peut la spécifier une seule fois

    }

}