var game;

window.onload = function() {

    let gameConfig = { 
        backgroundColor:0x87ceeb,
        scale: {
            autoCenter: Phaser.Scale.CENTER_BOTH,
            width: constantes.largeurJeu,
            height: constantes.hauteurJeu,
        },
        pixelArt: true,
        scene: playGame,
        physics: {
            default: 'arcade',
            arcade: {
                gravity: {
                    y: constantes.graviteJeuY,
                }
            },
        },
    }

    game = new Phaser.Game(gameConfig);
}

// on peut créer un objet pour stocker quelques constantes ou autre variables dans un endroit central
var constantes = {

    // la largeur du jeu
    largeurJeu: 1024,

    // la hauteur du jeu
    hauteurJeu: 480,

    // la couleur du fond du jeu
    couleurDuFond: 0x87ceeb,

    // la position des boutons depuis le bas de la fenêtre
    hauteurBoutonsBas: 10,

    // le nombre de pixels entre les deux boutons
    separationBoutons: 200,

    // est-ce que l'on stocke la valeur dans le navigateur de celui qui affiche le jeu
    stockerLaValeur: false,

    // le nom dans lequel stocker la valeur dans le navigateur
    nomDansLeNavigateur: '32-stocker-la-valeur',

}

class playGame extends Phaser.Scene {

    preload(){
        this.load.image('plus', 'assets/plus.png');
        this.load.image('moins', 'assets/moins.png');
    }

    create(){

        this.valeur = 0;

        // si on stocke la valeur et qu'il y a déjà une valeur de stockée, la récupérer
        if (constantes.stockerLaValeur) {
            let valeurStockee = localStorage.getItem(constantes.nomDansLeNavigateur);
            if (valeurStockee) {
                this.valeur = valeurStockee;
            }
        }

        // on ajoute un texte pour afficher la valeur
        this.valeurText = this.add.text(game.config.width / 2, game.config.height / 2, '').setOrigin(0.5);

        // on crée les deux boutons + et -
        this.moinsBtn = this.add.sprite((game.config.width - constantes.separationBoutons) / 2, game.config.height - constantes.hauteurBoutonsBas, 'moins').setOrigin(1, 1).setInteractive();
        this.plusBtn = this.add.sprite((game.config.width + constantes.separationBoutons) / 2, game.config.height - constantes.hauteurBoutonsBas, 'plus').setOrigin(0, 1).setInteractive();

        this.moinsBtn.on('pointerup', this.moins, this);
        this.plusBtn.on('pointerup', this.plus, this);
        
    }

    plus(){
        this.valeur++;
        this.stocker();
    }

    moins(){
        this.valeur--;
        this.stocker();
    }

    stocker(){
        if (constantes.stockerLaValeur) {
            localStorage.setItem(constantes.nomDansLeNavigateur, this.valeur);
        }
    }

    update() {
        // on affiche la valeur
        this.valeurText.text = 'Valeur: ' + this.valeur;
    }

}