var game;

window.onload = function() {

    let gameConfig = { 
        backgroundColor:0x87ceeb,
        scale: {
            autoCenter: Phaser.Scale.CENTER_BOTH,
            width: constantes.largeurJeu,
            height: constantes.hauteurJeu,
        },
        pixelArt: true,
        scene: playGame,
        physics: {
            default: 'arcade',
            arcade: {
                gravity: {
                    y: constantes.graviteJeuY,
                }
            },
        },
    }

    game = new Phaser.Game(gameConfig);
}

// on peut créer un objet pour stocker quelques constantes ou autre variables dans un endroit central
var constantes = {

    // la largeur du jeu
    largeurJeu: 1024,

    // la hauteur du jeu
    hauteurJeu: 480,

    // la couleur du fond du jeu
    couleurDuFond: 0x87ceeb,

    // la position d'origine de l'indicateur de vitesse en X
    positionIndicateurOrigineX: 10,

    // la position d'origine de l'indicateur de vitesse en Y
    positionIndicateurOrigineY: 50,

    // la position du texte indiquant la vitesse actuelle en X
    positionVitesseTextX: 10,

    // la position du texte indiquant la vitesse actuelle en Y
    positionVitesseTextY: 10,

    // la gravité de l'indicateur (i.e. l'accélération à laquelle la vitesse revient à 0)
    graviteIndicateurX: 200,

    // la vitesse de l'indicateur (i.e. l'impulsion que l'on donne quand on clique sur la souris pour
    // augmenter la vitesse)
    vitesseIndicateurX: 100,

    // la gravité du jeu en Y
    graviteJeuY: 300,

}

class playGame extends Phaser.Scene {

    preload(){
        this.load.image('indicateur', 'assets/indicateur.png');
        this.load.image('wall', 'assets/wall.png');
    }

    create(){

        // on crée un indicateur de vitesse que l'on va masquer
        this.indicateur = this.physics.add.sprite(constantes.positionIndicateurOrigineX, constantes.positionIndicateurOrigineY, 'indicateur').setOrigin(0, 1);
        
        // on masque l'indicateur, il ne sert pas d'un point de vue visuel dans notre cas
        this.indicateur.setVisible(false);

        // on crée des plateformes pour contenir l'indicateur de vitesse dans une zone donnée, que l'on masque également
        // les plateformes ne doivent pas être sensible à la gravité, on les met donc dans un groupe statique
        // elles font en effet partie de la physique du jeu puisque l'indicateur va devoir interagir avec elles
        this.platforms = this.physics.add.staticGroup();
        
        // on crée la partie horizontale à partir du "wall" (un carré vert de 5px x 5px à la base), que l'on modifie avec setScale pour l'agrandir
        this.platforms.create(constantes.positionIndicateurOrigineX, constantes.positionIndicateurOrigineY, 'wall').setOrigin(0).setScale(200, 1).refreshBody();
        
        // on crée la partie veticale à gauche à partir du "wall" (un carré vert de 5px x 5px à la base), que l'on modifie avec setScale pour l'agrandir
        this.platforms.create(constantes.positionIndicateurOrigineX, constantes.positionIndicateurOrigineY, 'wall').setOrigin(1,1).setScale(1, 2).refreshBody();
        
        // on masque les plateformes, elles ne servent pas d'un point de vue visuel
        this.platforms.setVisible(false);

        // on indique au moteur du jeu qu'il doit gérer les collisions entre l'indicateur et les plateformes
        this.physics.add.collider(this.indicateur, this.platforms);

        // on ajoute un texte pour afficher la vitesse
        this.vitesseText = this.add.text(constantes.positionVitesseTextX, constantes.positionVitesseTextY, '').setOrigin(0);
        
        // quand on clique sur la souris, on augmente la vitesse
        this.input.on('pointerdown', this.augmenterVitesse, this);
        
    }

    augmenterVitesse(){
        // un petit calcul qui fait que plus l'indicateur de vitesse est loin de sa position d'origine, plus la vitesse qu'on lui donne sera faible
        // autrement dit plus il sera difficile de continuer à "accélérer"
        this.indicateur.body.velocity.x = constantes.vitesseIndicateurX - (this.indicateur.x - constantes.positionIndicateurOrigineX) / 3  ;

        // la gravité est constante par contre
        this.indicateur.body.gravity.x = -constantes.graviteIndicateurX;
    }

    update() {
        // on calcule la vitesse à partir de la position de l'indicateur par rapport à sa position d'origine
        this.vitesseValeur =  Math.round(this.indicateur.x - constantes.positionIndicateurOrigineX)
        
        // on affiche la vitesse calculée
        this.vitesseText.text = 'Vitesse: ' + this.vitesseValeur;
    }

}