var game;

window.onload = function() {

    let gameConfig = { 
        backgroundColor:0x87ceeb,
        scale: {
            autoCenter: Phaser.Scale.CENTER_BOTH,
            width: 1024,
            height: 480
        },
        pixelArt: true,
        scene: playGame 
    }

    game = new Phaser.Game(gameConfig);
}

class playGame extends Phaser.Scene {

    // La fonction "create" est appelée une fois au démarrage du jeu
    create(){

        // on ajoute un texte vide, mais on stocke l'objet résultant (i.e. la "chose") dans la propriété
        // 'compteurText' de "this" qui est par convention l'objet dans lequel ce code est écrit (ici la scène)
        this.compteurText = this.add.text(game.config.width / 2, game.config.height / 2, '').setOrigin(0.5);

        // on stocke également un compteur sous forme d'entier comme propiété 'compteurValeur',
        // qui cette fois contient l'entier 0 au démarrage
        this.compteurValeur = 0;

    }

    // La fonction "update" est appelée à chaque fois que le jeu va raffraichir l'écran
    update(){

        // on incrémente la valeur du compteur de 1
        this.compteurValeur = this.compteurValeur + 1

        // on met à jour l'affichage de la valeur du compteur via la propriété 'text' de l'objet 
        // précédement stocké dans la propriété 'compteurText' de l'objet courant (la scène)
        this.compteurText.text = 'Compteur : ' + this.compteurValeur;

        // remarque : on voit ici une petite particularité de Javascript, l'opérateur '+' ne va pas
        // faire la même chose suivant ce sur quoi il s'applique :
        // - sur deux nombres, il fait la somme arithmétique
        // - sur deux chaînes de caractères, ou une chaîne de caractère et un nombre, il fait la concaténation,
        // i.e. il colle ensemble les deux morceaux pour faire une chaîne de caractère à partir des deux
        // morceaux 
    
    }

}