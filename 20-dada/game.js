var game;

window.onload = function() {

    let gameConfig = { 
        backgroundColor:0x87ceeb,
        scale: {
            autoCenter: Phaser.Scale.CENTER_BOTH,
            width: constantes.largeurJeu,
            height: constantes.hauteurJeu,
        },
        pixelArt: true,
        scene: playGame,
        physics: {
            default: 'arcade',
            arcade: {
                gravity: {
                    y: constantes.graviteJeuY,
                }
            },
        },
    }

    game = new Phaser.Game(gameConfig);
}

// on peut créer un objet pour stocker quelques constantes ou autre variables dans un endroit central
var constantes = {

    // la largeur du jeu
    largeurJeu: 1024,

    // la hauteur du jeu
    hauteurJeu: 480,

    // la couleur du fond du jeu
    couleurDuFond: 0x87ceeb,

    // la position d'origine du dada en X
    positionDadaOrigineX: 100,

    // la position d'origine du bouton pour recommencer en X en partant de la droite de l'écran
    positionRecommencerOrigineXDroite: 10,

    // la position d'origine du bouton pour recommencer en Y
    positionRecommencerOrigineY: 10,

    // la vitesse du dada en X quand on appuie sur les flèches droite et gauche
    vitesseDadaX: 200,

    // la gravité du jeu en Y
    graviteJeuY: 300,

}

class playGame extends Phaser.Scene {

    preload(){
        this.load.image('dada', 'assets/dada.png');
        this.load.image('recommencer', 'assets/recommencer.png');
    }

    create(){

        // notre cheval, comme d'habitude
        this.dada = this.physics.add.sprite(constantes.positionDadaOrigineX, game.config.height, 'dada')
            .setOrigin(0,1).setCollideWorldBounds(true);
        
        // une propriété pour savoir si le jeu est fini
        this.jeuFini = false;
        
        // quand on clique sur la souris, on fait avancer le dada
        this.input.on('pointerdown', this.faireAvancerDada, this);

        // quand on arrête de cliquer, le dada s'arrête
        this.input.on('pointerup', this.arreterDada, this);

        // on a un bouton pour redémarrer le jeu
        this.recommencerBtn = this.add.sprite(game.config.width - constantes.positionRecommencerOrigineXDroite, constantes.positionRecommencerOrigineY, 'recommencer')
            .setOrigin(1,0).setInteractive();

        // quand on clique sur le bouton "recommencer"
        this.recommencerBtn.once('pointerup', function() {
            this.scene.restart(); // on fait redémarrer la scène
        }, this);

    }

    faireAvancerDada(){
        if(!this.jeuFini) { // si le jeu n'est pas déjà fini
            this.dada.setVelocityX(constantes.vitesseDadaX); // on donne une vitesse au cheval, qui va donc avancer jusqu'à la ligne d'arrivée
        }
    }

    arreterDada(){
        this.dada.setVelocityX(0);
    }

    update() {
        if(this.dada.x >= game.config.width - constantes.positionDadaOrigineX ){
            this.jeuFini = true; // on indique que le dada ne doit plus pouvoir avancer
            this.arreterDada(); // on arrête le dada
        }
    }

}