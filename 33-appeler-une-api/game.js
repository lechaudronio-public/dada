var game;

window.onload = function() {

    let gameConfig = { 
        backgroundColor:0x87ceeb,
        scale: {
            autoCenter: Phaser.Scale.CENTER_BOTH,
            width: constantes.largeurJeu,
            height: constantes.hauteurJeu,
        },
        pixelArt: true,
        scene: playGame
    }

    game = new Phaser.Game(gameConfig);
}

var constantes = {

    // la largeur du jeu
    largeurJeu: 1024,

    // la hauteur du jeu
    hauteurJeu: 480,

    // la couleur du fond du jeu
    couleurDuFond: 0x87ceeb,

}

class playGame extends Phaser.Scene {

    create(){

        this.time.addEvent({
            delay: 2000, // au bout de combien de temps le timer va "sonner"
            callback: this.mettreDateHeureAJour, // la fonction à appeler quand le timer "sonne"
            callbackScope: this,
            loop: true, // le timer redémarre automatiquement
        });

        // on ajoute un texte pour afficher la valeur
        this.valeurText = this.add.text(game.config.width / 2, game.config.height / 2, '').setOrigin(0.5);

        this.mettreDateHeureAJour();        
    }

    mettreDateHeureAJour() {
        let req = new XMLHttpRequest();

        // on fait une requête de type "GET" sur l'URL ci-dessous
        // !!! Attention !!!
        // il faut penser à remplacer "UNECLESECRETE" par la clé qui vous a été donnée pour accéder à timezonedb.com
        // !!! Attention !!!
        req.open("GET", "https://api.timezonedb.com/v2.1/get-time-zone?key=UNECLESECRETE&format=json&by=zone&zone=Europe/Paris");

        let obj = this; // "truc" pour pouvoir utiliser "this" la fonction ci-dessous

        req.onreadystatechange = function() {
            if(req.readyState === XMLHttpRequest.DONE) {
                // la requête est terminée
                var status = req.status;
                if (status === 0 || (status >= 200 && status < 400)) {
                  // la requête s'est bien passée
                  let responseObj = JSON.parse(req.responseText);
                  if (responseObj.status != "OK") {
                      console.log("timezonedb.com a retourné un statut d'erreur (" + responseObj.status + ") : " + responseObj.message)
                  } else {
                    obj.valeur = responseObj.formatted;
                  }
                } else {
                  // Il y a eu une erreur avec la requête
                  console.log('Request failed with status ' + status)
                  console.log(req.responseText);
                }
            }
        };
        
        req.send();
    }

    update() {
        // on affiche la valeur si elle a déjà été initialisée au moins une fois
        if (this.valeur) {
            this.valeurText.text = this.valeur;
        }
        
    }

}