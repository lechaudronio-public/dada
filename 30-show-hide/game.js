var game;

window.onload = function() {

    let gameConfig = { 
        backgroundColor:0x87ceeb,
        scale: {
            autoCenter: Phaser.Scale.CENTER_BOTH,
            width: 1024,
            height: 480
        },
        pixelArt: true,
        scene: playGame,
        physics: {
            default: 'arcade',
            arcade: {
                gravity: {
                    y: 300
                }
            }
        },
    }

    game = new Phaser.Game(gameConfig);
}

var constantes = {
    positionTexteY: 50,
}

class playGame extends Phaser.Scene {

    preload(){
        this.load.image('arbre', 'assets/arbre.png'); 
    }

    create(){

        this.arbre = this.add.sprite(game.config.width / 2, game.config.height, 'arbre').setOrigin(0.5, 1);

        this.estVisible = true;

        this.texte = this.add.text(game.config.width / 2, constantes.positionTexteY, 'Cacher')
            .setOrigin(0.5, 0).setInteractive();

        this.texte.on('pointerup', this.basculerAffichage, this);

    }

    // modifier l'afficage de l'arbre
    basculerAffichage(){
        this.estVisible = !this.estVisible; // le ! signifie "Not", i.e. le contraire, i.e. si c'était vrai, ça devient faux, et inverserment
        
        this.arbre.visible = this.estVisible; // c'est avec la propriété "visible" (qui existe sur les sprites, le texte, ...) que l'on règle la visibilité des éléments
        
        // on ajuste le texte en fonction
        if (this.estVisible) {
            this.texte.text = 'Cacher'
        } else {
            this.texte.text = 'Afficher'
        }
    }


}