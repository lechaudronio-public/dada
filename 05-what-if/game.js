var game;

window.onload = function() {

    let gameConfig = { 
        backgroundColor:0x87ceeb,
        scale: {
            autoCenter: Phaser.Scale.CENTER_BOTH,
            width: constantes.largeurJeu,
            height: constantes.hauteurJeu,
        },
        pixelArt: true,
        scene: playGame,
        physics: {
            default: 'arcade',
            arcade: {
                gravity: {
                    y: constantes.graviteJeuY,
                }
            },
        },
    }

    game = new Phaser.Game(gameConfig);
}

// on peut créer un objet pour stocker quelques constantes ou autre variables dans un endroit central
var constantes = {

    // la largeur du jeu
    largeurJeu: 1024,

    // la hauteur du jeu
    hauteurJeu: 480,

    // la couleur du fond du jeu
    couleurDuFond: 0x87ceeb,

    // la position d'origine du dada en X
    positionDadaOrigineX: 100,

    // la position d'origine du dada en Y
    positionDadaOrigineY: 100,

    // la vitesse du dada en X quand on appuie sur les flèches droite et gauche
    vitesseDadaX: 200,

    // la vitesse du dada en Y quand on appuie sur la flèche haut
    vitesseDadaY: 300,

    // la gravité du dada en X pour simuler un "freinage"
    graviteDadaX: 100,

    // la gravité du jeu en Y
    graviteJeuY: 300,

    // la vitesse en X à partir duquel on considère que le dada est arrêté
    deltaVitesseXNulle: 10,

    // le taux de rebond du dada contre les murs
    tauxDeRebondDuDada: 0.5,

}

class playGame extends Phaser.Scene {

    preload(){
        this.load.image('dada', 'assets/dada.png');
    }

    create(){
        
        this.dada = this.physics.add.sprite(constantes.positionDadaOrigineX, constantes.positionDadaOrigineY, 'dada')
            .setOrigin(0,1) // le cheval est positionné par rapport à son milieu en bas (sert pour savoir quand est-ce qu'il touche le sol)
            .setCollideWorldBounds(true) // le cheval rebondit contre les murs dans toutes les directions
            .setBounce(constantes.tauxDeRebondDuDada); // en perdant à chaque fois la moitié de sa vitesse (si la constante est bien toujours 0.5)

        // une méthode pour récupérer les évènements du clavier (au lieu de la méthode vu dans 04-first-sprites pour
        // la souris ; plus efficace pour gérer les priorités entre les touches
        this.cursors = this.input.keyboard.createCursorKeys();
    }

    update()
    {
        // si on appuie sur la touche gauche, la vitesse en X du cheval devient négative
        // sinon si on appuie sur la touche droite, la vitesse en X du cheval devient positive
        // et donc la touche gauche a la priorité sur la touche droite
        if (this.cursors.left.isDown)
        {
            this.dada.setVelocityX(-constantes.vitesseDadaX);
        }
        else if (this.cursors.right.isDown)
        {
            this.dada.setVelocityX(constantes.vitesseDadaX);
        }

        // un cas de if / elseif / else
        // un cas où on manipule la gravité en X pour simuler "une résistance au mouvement"
        // si la vitesse en X est significativement positive, on freine le dada avec une gravité en X négative
        // inversement si la vitesse en X est significativement positive
        // sinon on arrête le déplacement en X du cheval 
        // (le "significativement" évite le truc où le cheval oscille quand il est sur le point de s'arrêter
        // car sa vitesse ne devient jamais tout à fait nulle)
        if (this.dada.body.velocity.x > constantes.deltaVitesseXNulle) {
            this.dada.setGravityX(-constantes.graviteDadaX);
        } else if (this.dada.body.velocity.x < -constantes.deltaVitesseXNulle) {
            this.dada.setGravityX(constantes.graviteDadaX);
        } else  {
            this.dada.setGravityX(0);
            this.dada.setVelocityX(0);
        }
        
        // si on appuie sur la touche haut et que le cheval touche le sol, la vitesse en Y du cheval
        // devient négative (i.e. il saute)
        if (this.cursors.up.isDown && this.dada.y == this.game.config.height)
        {
            this.dada.setVelocityY(-constantes.vitesseDadaY);
        }

    }

}