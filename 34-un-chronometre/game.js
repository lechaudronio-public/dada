var game;

window.onload = function() {

    let gameConfig = { 
        backgroundColor:0x87ceeb,
        scale: {
            autoCenter: Phaser.Scale.CENTER_BOTH,
            width: constantes.largeurJeu,
            height: constantes.hauteurJeu,
        },
        pixelArt: true,
        scene: playGame
    }

    game = new Phaser.Game(gameConfig);
}

var constantes = {

    // la largeur du jeu
    largeurJeu: 1024,

    // la hauteur du jeu
    hauteurJeu: 480,

    // la couleur du fond du jeu
    couleurDuFond: 0x87ceeb,

}

class playGame extends Phaser.Scene {

    create(){
        // on ajoute un texte pour afficher la valeur
        this.valeurText = this.add.text(game.config.width / 2, game.config.height / 2, '').setOrigin(0.5);

        // un texte en guise de bouton pour démarrer / arrêter le chronomètre
        this.boutonText = this.add.text(game.config.width / 2, game.config.height / 2 - 20, 'Démarrer').setOrigin(0.5).setInteractive();
        this.boutonText.on('pointerup', this.toggleTimer, this);
    }

    toggleTimer() {
        if (!this.timer) {
            // Si on n'a pas encore de timer, c'est qu'il faut lancer le chronomètre
            this.timer = this.time.addEvent({ delay: 1, loop: true });
            this.boutonText.text = "Arrêter";
        } else {
            // Sinon on supprime le timer
            this.timer = null;
            this.boutonText.text = "Démarrer";
        }
    }

    update() {
        if (this.timer) {
            // si le timer existe, on peut extraire la valeur dans un chronomètre (nécessaire pour garder
            // la valeur affichée même une fois que le timer est supprimé)
            this.chrono = Math.round(this.timer.getOverallProgress()) / 1000;
        }
        if (this.chrono) {
            // si on a la valeur, alors on l'affiche
            this.valeurText.text = this.prettyPrint(this.chrono);
        }
    }

    prettyPrint(time) {
        // on ajoute des zéros à la fin pour toujours avoir 3 décimales d'affichées et avoir un truc qui bouge moins sans arrêt à l'écran :
        // 0.123 => 0.123
        // 0.12 => 0.120
        // 1.23 => 1.230
        // 10.25 => 10.250
        // 11.156 => 11.156
        // ...
        return (time + '').padEnd(this.getNombreDeCaracteresPour3Decimales(time), 0);
    }

    getNombreDeCaracteresPour3Decimales(time) {
        if (time < 1) {
            return 5;
        } else {
            return 5 + Math.floor(Math.log10(time));
        }
    }

}