var game;

window.onload = function() {

    let gameConfig = { 
        backgroundColor:0x87ceeb,
        scale: {
            autoCenter: Phaser.Scale.CENTER_BOTH,
            width: 1024,
            height: 480
        },
        pixelArt: true,
        scene: playGame,
        physics: {
            default: 'arcade',
            arcade: {
                gravity: {
                    y: 300
                }
            }
        },
    }

    game = new Phaser.Game(gameConfig);
}

class playGame extends Phaser.Scene {

    // La fonction "preload" permet de précharger les assets graphiques par exemple
    preload(){
        this.load.image('arbre', 'assets/arbre.png'); // on charge dans un asset nommé "arbre" l'image stockée dans assets/arbre.png
        this.load.image('dada', 'assets/dada.png');
        this.load.image('moins', 'assets/moins.png');
        this.load.image('plus', 'assets/plus.png');
    }

    // La fonction "create" est appelée une fois au démarrage du jeu
    create(){

        this.compteurValeur = 0;

        // On dessine un arbre au milieu en bas du jeu. Pour cela, on change l'origine de l'arbre pour
        // être le point au milieu en bas de l'arbre et on positionne ce point au milieu en bas de l'image
        this.arbre = this.add.sprite(game.config.width / 2, game.config.height, 'arbre').setOrigin(0.5, 1);

        // le dada n'est pas un sprite classique, il subit la physique du jeu, on l'ajoute donc à la propriété 'physics'
        // avec 'setCollideWorldBounds(true)' on indique que quand le dada rentre en collision avec 
        // les bords du jeu, il s'arrête
        this.dada = this.physics.add.sprite(100, 100, 'dada').setCollideWorldBounds(true); 

        // les boutons plus et moins sont des sprites qui ne subissent pas la physique du jeu, mais
        // par contre on indique qu'ils sont interactifs avec setInteractive, ainsi le jeu va observer
        // les évènements (de souris) qui se passent sur ces sprites
        this.moinsBtn = this.add.sprite(game.config.width / 2 - 100, game.config.height - 10 , 'moins').setOrigin(1, 1).setInteractive();
        this.plusBtn = this.add.sprite(game.config.width / 2 + 100, game.config.height - 10 , 'plus').setOrigin(0, 1).setInteractive();

        // quand l'évènement 'pointeurup' (i.e. que l'utilisateur relache le bouton du pointeur de la souris sur le 'moinsBtn',
        // on exécute la fonction passée en paramètre, directement "en ligne" ici
        // le dernier paramètre est nécessaire pour dire que le "this" dans le code de la fonction "en ligne" est le même objet
        // que le "this" en  dehors de la fonction
        this.moinsBtn.on('pointerup', function () {
            this.compteurValeur = this.compteurValeur - 1; // une autre syntaxe plus courte quand on a besoin de décrémenter que de 1
            console.log(this.compteurValeur); // pour débugger, on peut afficher du contenu dans la console de votre navigateur (à ne pas laisser au final)
        }, this);

        // quand l'évènement 'pointeurup' (i.e. que l'utilisateur relache le bouton du pointeur de la souris sur le 'plusBtn',
        // on exécute la fonction passée en paramètre, définie ailleurs dans l'object de la scène
        this.plusBtn.on('pointerup', this.plus, this);

    }

    // on peut définir de nouvelles fonctions à volonter
    plus(){
        this.compteurValeur++; // une autre syntaxe plus courte quand on a besoin d'incrémenter que de 1
        console.log(this.compteurValeur); // pour débugger, on peut afficher du contenu dans la console de votre navigateur (à ne pas laisser au final)
    }


}